<?php
/*
Plugin Name: Asm web
Plugin URI: https://www.quantr.hk
Description: Portal to manage instruction translation
Version: 1.0.0
Author: Peter
Support URI: https://www.quantr.hk
Author URI: https://peter.quantr.hk
License: Quantr Commercial License

{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

include(ABSPATH . '/wp-includes/pluggable.php');
include(plugin_dir_path(__FILE__) . 'public/shortcode.php');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

global $wpdb;
$charset_collate = $wpdb->get_charset_collate();

define("GITLAB_ACCESS_TOKEN", "glpat-RtXSSxEevVY9ys3r1i7c");
define("ASSEMBLER_PROJECT_ID", "4572912");

define("ASSEMBLER_USERNAME","assembler");
define("ASSEMBLER_PASSWORD","assemblerasldjaklsjdsajd1209830918!@#!@#SD");
define("ASSEMBLER_DB","assembler");
define("ASSEMBLER_HOST","localhost");

define('NASM_TABLE_PREFIX', 'nasm_');
define('nasm_ia32_instruction', $wpdb->prefix.NASM_TABLE_PREFIX.'ia32_instruction');
$sql = "CREATE TABLE ".nasm_ia32_instruction." (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  createDate datetime NULL,
  code varchar(100) NOT NULL,
  bits16 varchar(100) NULL,
  bits32 varchar(100) NULL,
  bits64 varchar(100) NULL,
  quantrBits16 varchar(100) NULL,
  quantrBits32 varchar(100) NULL,
  quantrBits64 varchar(100) NULL,
  pass BOOL NULL default false,
  passDate datetime NULL,
  PRIMARY KEY (id)
) $charset_collate;";

// dbDelta($sql);

define('nasm_ia32_instruction_test', $wpdb->prefix.NASM_TABLE_PREFIX.'ia32_instruction_test');
$sql_test = "CREATE TABLE ".nasm_ia32_instruction_test." (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  createDate datetime NULL,
  func varchar(100) NOT NULL,
  code varchar(100) NOT NULL,
  bits16 varchar(100) NULL,
  bits32 varchar(100) NULL,
  bits64 varchar(100) NULL,
  quantrBits16 varchar(100) NULL,
  quantrBits32 varchar(100) NULL,
  quantrBits64 varchar(100) NULL,
  pass BOOL NULL default false,
  passDate datetime NULL,
  PRIMARY KEY (id)
) $charset_collate;";

// dbDelta($sql_test);

define('nasm_ia32_instruction_chart', $wpdb->prefix.NASM_TABLE_PREFIX.'ia32_instruction_chart');
$sql_chart = "CREATE TABLE ".nasm_ia32_instruction_chart." (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  createDate datetime NULL,
  pass int default 0,
  fail int default 0,
  total int default 0,
  content longtext default null,
  PRIMARY KEY (id)
) $charset_collate;";

// dbDelta($sql_chart);
?>
