<link href="https://fonts.googleapis.com/css2?family=Lato&family=Montserrat&family=Open+Sans&family=Roboto&family=Source+Code+Pro:ital,wght@0,200;0,300;0,400;0,500;0,600;1,200;1,300;1,400;1,500&family=Source+Sans+Pro&display=swap" rel="stylesheet">

<style>
	.byteDiv {
		font-family: 'Source Code Pro', monospace;
	}

	.byteBlock {
		cursor: pointer;
	}

	.byteBlock:hover {
		background-color: aliceblue;
	}

	.highlightByte {
		background-color: #fbe1ff;
	}

	.addressDiv {
		background-color: #dfdfdf;
		font-family: 'Source Code Pro', monospace;
	}

	#disasmResult {
		font-family: 'Source Code Pro', monospace;
		font-size: 100%;
	}
</style>
<?php
// require '../../../../wp-config.php';
define("MAX_NO_BYTE", 16 * 4);

$a = array("33", "10", "01", "00");
?>
<div class="container">
	<div class="row">
		<div class="col-2">
			<table class="table addressDiv">
				<?php for ($x = 0; $x < MAX_NO_BYTE; $x += 16) { ?>
					<tr>
						<td class="addressBlock" style="text-align: center;">
							<?= str_pad(dechex($x), 8, "0", STR_PAD_LEFT) ?>
						</td>
					</tr>
				<?php } ?>
			</table>
		</div>
		<div class="col-8">
			<table class="table byteDiv mb-0">
				<?php for ($x = 0; $x < MAX_NO_BYTE; $x++) { ?>
					<?php if ($x != 0 && $x % 16 == 0) { ?>
						</tr>
					<?php } ?>
					<?php if ($x % 16 == 0) { ?>
						<tr>
						<?php } ?>
						<td class="byteBlock" tabindex="0">
							<?= str_pad($a[$x % count($a)], 2, "0", STR_PAD_LEFT) ?>
						</td>
					<?php } ?>
			</table>
		</div>
		<div class="col-2">
			<input type="file" id="fileUploaderBinary" onchange="loadBinary();" />
			<button type="button" onclick="loadBinary();" class="btn btn-secondary mb-2 mt-2" style="width: 100%; height: 30%;">Load binary file</button>
			<!-- <button type="button" class="btn btn-secondary mb-2" style="width: 100%; height: 30%;">Load elf file</button> -->
			<input type="radio" class="form-input" name="disassembleArch" id="disassembleArch" value="rv32" checked> RV32
			<input type="radio" class="form-input" name="disassembleArch" id="disassembleArch" value="rv64"> RV64
			<button type="button" class="btn btn-secondary" style="width: 100%; margin-bottom: 2px;" onclick="disassembleQuantr($('#disassembleArch:checked').val());">Quantr Disasm</button>
			<button type="button" class="btn btn-secondary" style="width: 100%; margin-bottom: 2px;" onclick="disassembleGas($('#disassembleArch:checked').val());">Gas Disasm</button>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div style="text-align: center">
				<img id="disassemblerLoading" style="display: none;" src="<?php echo plugin_dir_url(__FILE__) ?>/images/Spinner-1.1s-194px.svg" />
			</div>
			<pre id="disasmResult"></pre>
		</div>
	</div>
</div>
<script>
	function loadBinary() {
		$('#disasmResult').html(null);
		$('#disassemblerLoading').css('display', '');

		var file_data = $('#fileUploaderBinary').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		$.ajax({
			url: '<?php echo plugin_dir_url(__FILE__) ?>/uploadDisasmBinary.php',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(data) {
				$('#disasmResult').html(data);
				$('#disassemblerLoading').css('display', 'none');
			}
		});
	}

	function disassembleQuantr(arch) {
		$('#disasmResult').html(null);
		$('#disassemblerLoading').css('display', '');
		var str = "";
		$(".byteBlock").each(function(index, elem) {
			str += elem.innerHTML.trim();
		});

		$.ajax({
			async: true,
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execQuantrDisassembler.php?arch=' + arch + '&bytesStr=' + str,
			success: function(data) {
				$('#disasmResult').html(data);
				$('#disassemblerLoading').css('display', 'none');
			}
		});
	}

	function disassembleGas(arch) {
		$('#disasmResult').html(null);
		$('#disassemblerLoading').css('display', '');
		var str = "";
		$(".byteBlock").each(function(index, elem) {
			str += elem.innerHTML.trim();
		});

		$.ajax({
			async: true,
			url: '<?php echo plugin_dir_url(__FILE__) ?>/execGasDisassembler.php?arch=' + arch + '&bytesStr=' + str,
			success: function(data) {
				$('#disasmResult').html(data);
				$('#disassemblerLoading').css('display', 'none');
			}
		});
	}

	$(function() {
		var temp = "";

		$(".byteBlock").click(function() {
			$(".byteBlock").removeClass("highlightByte");
			$(this).addClass("highlightByte");
			temp = ""
		});

		$(".byteBlock").bind("paste", function(e) {
			var pastedData = e.originalEvent.clipboardData.getData('text');
			// alert(pastedData);

			for (x = 0; x < pastedData.length; x++) {
				var keyup = jQuery.Event('keyup');
				keyup.key = pastedData.charAt(x);
				keyup.keyCode = pastedData.charCodeAt(x);
				$('.highlightByte').trigger(keyup);
			}
		});

		$(".byteBlock").keyup(function(evt) {
			if (!((evt.keyCode >= 48 && evt.keyCode <= 57) || (evt.keyCode >= 65 && evt.keyCode <= 70))) {
				return false;
			}

			if ((temp + evt.key).length == 1) {
				$(this).html("0" + temp + evt.key);
			} else {
				$(this).html(temp + evt.key);
			}

			temp = temp + evt.key;

			if (temp.length == 2) {
				temp = ""
				if ($(this).closest('td').next().length == 0) {
					// next row
					$(this).closest('tr').next('tr').find('td:first').focus();
					$(".byteBlock").removeClass("highlightByte");
					$(this).closest('tr').next('tr').find('td:first').addClass("highlightByte");
				} else {
					$(this).closest('td').next().focus();
					$(".byteBlock").removeClass("highlightByte");
					$(this).closest('td').next().addClass("highlightByte");
				}
			}
		});
	});
</script>