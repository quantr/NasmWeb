<?
require '../../../../wp-config.php';

//$currentUser = wp_get_current_user();
//$username=$currentUser->user_login;

global $wpdb;

// $ch = curl_init();
// //curl_setopt($ch, CURLOPT_URL, "http://quantr-assembler.quantr.hk/compile?code=".urlencode("(bits ".$_GET['bits']."){".$results[0]->code."}")."&type=byte");
// curl_setopt($ch, CURLOPT_URL, "http://tomcat.quantr.hk/assembler/compile?code=".urlencode("(bits ".$_GET['bits']."){".$results[0]->code."}")."&type=byte");
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// $response=curl_exec($ch);
//
// $ch = curl_init();
// curl_setopt($ch, CURLOPT_URL, plugin_dir_url(__FILE__)."/save.php?id=".$_GET['id']."&bits=".$_GET['bits']."&data=".urlencode($response)."&type=quantrAssembler");
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// $response=curl_exec($ch);
//

$source="bits".$_GET['bits']."Temp.asm";
$results = $wpdb->get_results("SELECT * FROM ".nasm_ia32_instruction." where id=".$_GET['id'].";", OBJECT);

$file = fopen(sys_get_temp_dir()."/".$source, "w");
if ($_GET['bits']==16){
	fwrite($file, "(bits 16){\n");
}else if ($_GET['bits']==32){
	fwrite($file, "(bits 32){\n");
}else{
	fwrite($file, "(bits 64){\n");
}
fwrite($file, $results[0]->code);
fwrite($file, "}\n");
fclose($file);

$jdk_home = '/home/gitlab-runner/Downloads/jdk-15';
exec("$jdk_home/bin/java -jar Assembler*.jar ".sys_get_temp_dir()."/".$source." -o ".sys_get_temp_dir()."/temp.bin");

$hex = unpack("H*", file_get_contents(sys_get_temp_dir()."/temp.bin"));
$hex = current($hex);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, plugin_dir_url(__FILE__)."/save.php?id=".$_GET['id']."&bits=".$_GET['bits']."&data=".urlencode($hex)."&type=quantrAssembler");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response=curl_exec($ch);

echo "ok";
?>
