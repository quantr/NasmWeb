<?php
require '../../../../wp-config.php';

global $wpdb;
$wpdb->query('TRUNCATE TABLE '.nasm_ia32_instruction_test);

exec("java -jar Assembler*.jar -ft > ".sys_get_temp_dir()."/temp.bin");
$str=file_get_contents(sys_get_temp_dir()."/temp.bin");
$lines = explode("\n", $str);

$pass=-1;
$fail=-1;
$total=-1;

for ($x=0;$x<count($lines);$x++){
	if (strpos($lines[$x], 'pass=') !== false){
		$pass=explode('=',$lines[$x])[1];
	}else if (strpos($lines[$x], 'fail=') !== false){
		$fail=explode('=',$lines[$x])[1];
	}else if (strpos($lines[$x], 'total=') !== false){
		$total=explode('=',$lines[$x])[1];
	}else{
		$lines[$x]=str_replace('[31m','',$lines[$x]);
		$lines[$x]=str_replace('[34m','',$lines[$x]);
		$lines[$x]=str_replace('[m','',$lines[$x]);

		$arr=explode('|',$lines[$x]);
		$func=trim($arr[1]);
		$code=trim($arr[2]);
		$quantr16=trim($arr[3]);
		$nasm16=trim($arr[4]);
		$quantr32=trim($arr[5]);
		$nasm32=trim($arr[6]);
		$quantr64=trim($arr[7]);
		$nasm64=trim($arr[8]);
		$passed=($nasm16==$quantr16 && $nasm32==$quantr32 && $nasm64 == $quantr64);
		echo $lines[$x]."<br>";
		// echo 'p='.($pass?'t':'f')."<br>";
		// echo '1='.($nasm16)."<br>";
		// echo '2='.($quantr16)."<br>";
		// echo '3='.strlen(trim($nasm16))."<br>";
		// echo '4='.strlen(trim($quantr16))."<br>";

		if ($quantr16=='' || $code=='Code'){
			continue;
		}

		$wpdb->insert(
			nasm_ia32_instruction_test,
			array(
				'createDate' => current_time('mysql'),
				'func' => $func,
				'code' => $code,
				'bits16' => $nasm16,
				'bits32' => $nasm32,
				'bits64' => $nasm64,
				'quantrBits16' => $quantr16,
				'quantrBits32' => $quantr32,
				'quantrBits64' => $quantr64,
				'pass' => $passed,
				'passDate' => $passed?current_time('mysql'):null
			)
		);
	}
}

$wpdb->insert(
	nasm_ia32_instruction_chart,
	array(
		'createDate' => current_time('mysql'),
		'pass' => $pass,
		'fail' => $fail,
		'total' => $total,
		'content' => $wpdb->escape($str)
	)
);

echo "ok";
?>
