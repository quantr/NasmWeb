<?php
require '../../../../wp-config.php';

$pageSize = 1000;

$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
if (!$conn) {
	echo 'Could not connect: ' . mysqli_error($conn);
}

$sql = "desc cpuRecorder";
$stmt = mysqli_prepare($conn, $sql);
mysqli_stmt_execute($stmt);
$result = mysqli_stmt_get_result($stmt);
$fields=array();
while ($row = mysqli_fetch_assoc($result)) {
	array_push($fields, $row['Field']);
}

if ($_GET['mode']=='true'){
	unset($fields[array_search('id', $fields)]);
	unset($fields[array_search('guid', $fields)]);
	unset($fields[array_search('computer', $fields)]);
	unset($fields[array_search('date', $fields)]);
	unset($fields[array_search('pc', $fields)]);
	unset($fields[array_search('code', $fields)]);
	unset($fields[array_search('mcycle', $fields)]);
	unset($fields[array_search('minstret', $fields)]);
	unset($fields[array_search('cycle', $fields)]);
	unset($fields[array_search('instret', $fields)]);
	unset($fields[array_search('dpc', $fields)]);

	
	echo "<thead><tr>";
	// echo "<th>id</th>";
	echo "<th></th>";
	echo "<th>pc</th>";
	echo "<th>code</th>";
	echo "<th>mcycle</th>";
	echo "<th>minstret</th>";
	echo "<th>cycle</th>";
	echo "<th>instret</th>";
	echo "<th>dpc</th>";
	echo "</thead></tr>";

	$lastRow=array();

	$sql = "select * from cpuRecorder where guid=? order by id;";
	$stmt = mysqli_prepare($conn, $sql);

	mysqli_stmt_bind_param($stmt, "s", $_GET['guid']);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$rowNo=0;
	echo "<tbody>";
	while ($row = mysqli_fetch_assoc($result)) {
		echo "<tr>";
		// echo "<td>".$row['id']."</td>";
		echo "<td>".$rowNo++."</td>";
		echo "<td>".dechex($row['pc'])."</td>";
		echo "<td nowrap class='disasmCode'>".$row['code']."</td>";
		echo "<td>".$row['mcycle']."</td>";
		echo "<td>".$row['minstret']."</td>";
		echo "<td>".$row['cycle']."</td>";
		echo "<td>".$row['instret']."</td>";
		echo "<td class='rightBorderSimulator'>".dechex($row['dpc'])."</td>";
		foreach($fields as $field) {
			if ($lastRow[$field]!=$row[$field]){
			?>
				<td style="background-color: #e0f0f3;"><?= $field ?></td>
				<td><?= dechex($row[$field]) ?></td>
			<? } ?>
			<?
				$lastRow[$field]=$row[$field];
			?>
	<?	}
		echo "</tr>";
	}
	echo "</tbody>";
}else{
	unset($fields[array_search('id', $fields)]);
	unset($fields[array_search('guid', $fields)]);
	unset($fields[array_search('computer', $fields)]);
	unset($fields[array_search('date', $fields)]);
	unset($fields[array_search('pc', $fields)]);
	unset($fields[array_search('code', $fields)]);

	// array_splice($fields, 0, 0, 'id');
	// array_splice($fields, 1, 0, 'guid');
	// array_splice($fields, 2, 0, 'computer');
	// array_splice($fields, 3, 0, 'date');
	// array_splice($fields, 4, 0, 'pc');

	echo "<thead><tr>";
	echo "<th>id</th>";
	echo "<th>pc</th>";
	echo "<th>code</th>";
	foreach($fields as $field) {
		echo "<th>".$field."</th>";
	}
	echo "</thead></tr>";

	$sql = "select * from cpuRecorder where guid=? order by id asc;";
	$stmt = mysqli_prepare($conn, $sql);

	mysqli_stmt_bind_param($stmt, "s", $_GET['guid']);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	echo "<tbody>";
	while ($row = mysqli_fetch_assoc($result)) {
		echo "<tr>";
		echo "<td>".$row['id']."</td>";
		// echo "<td>".$row['guid']."</td>";
		// echo "<td>".$row['computer']."</td>";
		// echo "<td>".$row['date']."</td>";
		echo "<td>".dechex($row['pc'])."</td>";
		echo "<td nowrap class='rightBorderSimulator disasmCode'>".$row['code']."</td>";
		foreach($fields as $field) {
	?>
			<? if ($row[$field]==0){ ?>
				<td><?= dechex($row[$field]) ?></td>
			<? } else { ?>
				<td style="background-color: #e0f0f3;"><?= dechex($row[$field]) ?></td>
			<? } ?>
	<?	}
		echo "</tr>";
	}
	echo "</tbody>";
}
?>