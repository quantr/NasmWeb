<?php
require '../../../../wp-config.php';

$ch = curl_init();
$url = "https://gitlab.com/api/v4/projects/" . ASSEMBLER_PROJECT_ID . "/jobs?private_token=" . GITLAB_ACCESS_TOKEN . "&owned=true&visibility=public&per_page=1000";
// echo $url;
// die;
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);

// echo "<pre>" . $url . "</pre>";
// echo "<pre>" . $output . "</pre>";

curl_close($ch);
$json = json_decode($output);
// die;

// job
$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
if (!$conn) {
	echo 'Could not connect: ' . mysqli_error($conn);
}
$result = mysqli_query($conn, "select * from job order by datetime desc;");
$CI_JOB_IDs = array();
while ($row = mysqli_fetch_assoc($result)) {
	array_push($CI_JOB_IDs, $row['CI_JOB_ID']);
}
mysqli_close($conn);
// end job

function secondsToTime($inputSeconds)
{
	$secondsInAMinute = 60;
	$secondsInAnHour  = 60 * $secondsInAMinute;
	$secondsInADay    = 24 * $secondsInAnHour;

	// extract days
	$days = floor($inputSeconds / $secondsInADay);

	// extract hours
	$hourSeconds = $inputSeconds % $secondsInADay;
	$hours = floor($hourSeconds / $secondsInAnHour);

	// extract minutes
	$minuteSeconds = $hourSeconds % $secondsInAnHour;
	$minutes = floor($minuteSeconds / $secondsInAMinute);

	// extract the remaining seconds
	$remainingSeconds = $minuteSeconds % $secondsInAMinute;
	$seconds = ceil($remainingSeconds);

	// return the final array
	$obj = array(
		'd' => (int) $days,
		'h' => (int) $hours,
		'm' => (int) $minutes,
		's' => (int) $seconds,
	);
	return $obj;
}

function dateDiff($time1, $time2, $precision = 2)
{
	// If not numeric then convert timestamps
	if (!is_int($time1)) {
		$time1 = strtotime($time1);
	}
	if (!is_int($time2)) {
		$time2 = strtotime($time2);
	}
	// If time1 > time2 then swap the 2 values
	if ($time1 > $time2) {
		list($time1, $time2) = array($time2, $time1);
	}
	// Set up intervals and diffs arrays
	$intervals = array('year', 'month', 'day', 'hour', 'minute', 'second');
	$diffs = array();
	foreach ($intervals as $interval) {
		// Create temp time from time1 and interval
		$ttime = strtotime('+1 ' . $interval, $time1);
		// Set initial values
		$add = 1;
		$looped = 0;
		// Loop until temp time is smaller than time2
		while ($time2 >= $ttime) {
			// Create new temp time from time1 and interval
			$add++;
			$ttime = strtotime("+" . $add . " " . $interval, $time1);
			$looped++;
		}
		$time1 = strtotime("+" . $looped . " " . $interval, $time1);
		$diffs[$interval] = $looped;
	}
	$count = 0;
	$times = array();
	foreach ($diffs as $interval => $value) {
		// Break if we have needed precission
		if ($count >= $precision) {
			break;
		}
		// Add value and interval if value is bigger than 0
		if ($value > 0) {
			if ($value != 1) {
				$interval .= "s";
			}
			// Add value and interval to times array
			$times[] = $value . " " . $interval;
			$count++;
		}
	}
	// Return string with times
	return implode(", ", $times);
}

for ($x = 0; $x < count($json); $x++) {
	$job = $json[$x];
	// if ($job->stage != 'test') {
		// continue;
	// }
	if ($job->status == 'success') {
		$statusClass = "ci-success";
	} else if ($job->status == 'canceled') {
		$statusClass = "ci-canceled";
	} else if ($job->status == 'failed') {
		$statusClass = "ci-failed";
	}
	?>
	<tr>
		<td><span class="ci-status <?= $statusClass ?>"><?= $job->status ?></span></td>
		<td>
			#<?= $job->id ?>
			<i class="fas fa-code-branch"></i>
			<strong><?= $job->ref ?></strong>
			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
				<path d="M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3.876-1.008a4.002 4.002 0 0 1-7.752 0A1.01 1.01 0 0 1 4 9H1a1 1 0 1 1 0-2h3c.042 0 .083.003.124.008a4.002 4.002 0 0 1 7.752 0A1.01 1.01 0 0 1 12 7h3a1 1 0 0 1 0 2h-3a1.01 1.01 0 0 1-.124-.008z"></path>
			</svg>
			<a href="https://gitlab.com/quantr/toolchain/Assembler/commit/<?= $job->commit->id ?>" target="_blank"><?= $job->commit->short_id ?></a>
			<br>
			<span class="badge badge-primary"><?= $job->runner->description ?></span>
		</td>
		<td>
			<a href="https://gitlab.com/quantr/toolchain/Assembler/pipelines/<?= $job->pipeline->id ?>" target="_blank">#<?= $job->pipeline->id ?></a>
			<img src="<?= $job->user->avatar_url ?>" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #f5f5f5;" />
			<?= $job->user->name ?>
		</td>
		<td class="d-none d-sm-table-cell">
			<?= $job->stage ?>
		</td>
		<td class="d-none d-sm-table-cell">
			<pre style="width: 250px; white-space: pre-wrap; font-size:14px; margin: 0px;"><?= $job->commit->message ?></pre>
		</td>
		<td class="d-none d-sm-table-cell">
			<i class="fas fa-clock"></i> <?= secondsToTime($job->duration)['m'] . ':' . secondsToTime($job->duration)['s'] ?>
			<br>
			<i class="fas fa-calendar-alt"></i> <?= dateDiff(date('Y-m-d H:i:s'), str_replace('Z', ' ', str_replace('T', ' ', $job->finished_at))) ?>
		</td>
		<td class="d-none d-sm-table-cell">
			<?
				$path = '/home/gitlab-runner/output/output_' . $job->id . '.html';
				if (file_exists($path)) {
					?>
				<button type="button" class="btn btn-success" onclick="viewOutput('<?= $path ?>');">Old</button>
			<?
				}
				?>
			<? if (in_array($job->id, $CI_JOB_IDs)) { ?>
				<button type="button" class="btn btn-success" onclick="$('#pipelineResultTab').tab('show');">View</button>
			<? } ?>
		</td>
	</tr>
<?
}
?>