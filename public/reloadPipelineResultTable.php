<?php
require '../../../../wp-config.php';

$pageSize = 1000;

$conn = mysqli_connect(ASSEMBLER_HOST, ASSEMBLER_USERNAME, ASSEMBLER_PASSWORD, ASSEMBLER_DB);
if (!$conn) {
	echo 'Could not connect: ' . mysqli_error($conn);
}
if ($_GET['type'] == 'table') {
	if ($_GET['arch']=='IA32'){
		if ($_GET['selectType'] == 'error') {
			$where = " and ((not quantr16<=>nasm16 and (nasm16Error is null or nasm16Error='')) or (not quantr32<=>nasm32 and (nasm32Error is null or nasm32Error='')) or (not quantr64<=>nasm64 and (nasm64Error is null or nasm64Error='')))";
		} else if ($_GET['selectType'] == 'mis-encoded') {
			$where = ' and ((nasm16 is null and not quantr16 is null) or (nasm32 is null and not quantr32 is null) or (nasm64 is null and not quantr64 is null))';
		}
		if ($_GET['instruction'] != '') {
			$where .= " and SUBSTRING_INDEX(test.code, ' ', 1) = '" . $_GET['instruction'] . "'";
		}else if ($_GET['search'] != '') {
			$where .= " and SUBSTRING_INDEX(test.code, ' ', 1) = '" . $_GET['search'] . "'";
		}

		$sql = "select *,
					(case quantr16<=>nasm16 when true then 'y' else 'n' end) as c16,
					(case quantr32<=>nasm32 when true then 'y' else 'n' end) as c32,
					(case quantr64<=>nasm64 when true then 'y' else 'n' end) as c64,
					((not quantr16<=>nasm16 and (nasm16Error is null or nasm16Error='')) or (not quantr32<=>nasm32 and (nasm32Error is null or nasm32Error='')) or (not quantr64<=>nasm64 and (nasm64Error is null or nasm64Error=''))) as error,
					((nasm16 is null and not quantr16 is null) or (nasm32 is null and not quantr32 is null) or (nasm64 is null and not quantr64 is null)) as misEncode
					from test
					left join nasm
					on test.code=nasm.code
					where CI_JOB_ID=? $where
					order by test.id limit " . ($_GET['pageNo'] - 1) * $pageSize . ",$pageSize;";
		// echo $sql;
		// die;
		$stmt = mysqli_prepare($conn, $sql);

		mysqli_stmt_bind_param($stmt, "s", $_GET['CI_JOB_ID']);
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		$x = 1;
		while ($row = mysqli_fetch_assoc($result)) {
			?>
			<tr>
				<?
						$row['quantr16'] = str_replace('0x', '', $row['quantr16']);
						$row['quantr32'] = str_replace('0x', '', $row['quantr32']);
						$row['quantr64'] = str_replace('0x', '', $row['quantr64']);
						$row['nasm16'] = str_replace('0x', '', $row['nasm16']);
						$row['nasm32'] = str_replace('0x', '', $row['nasm32']);
						$row['nasm64'] = str_replace('0x', '', $row['nasm64']);
						$n16 = str_replace('0x', '', $row['nasm16']);
						$n32 = str_replace('0x', '', $row['nasm32']);
						$n64 = str_replace('0x', '', $row['nasm64']);
						if ($row['nasm16'] == null) {
							$n16 = '<pre>' . $row['nasm16Error'] . '</pre>';
							$q16 = $row['quantr16'];
						} else {
							$q16 = compare($row['quantr16'], $n16);
							$n16 = compare($n16, $row['quantr16']);
						}
						if ($row['nasm32'] == null) {
							$n32 = '<pre>' . $row['nasm32Error'] . '</pre>';
							$q32 = $row['quantr32'];
						} else {
							$q32 = compare($row['quantr32'], $n32);
							$n32 = compare($n32, $row['quantr32']);
						}
						if ($row['nasm64'] == null) {
							$n64 = '<pre>' . $row['nasm64Error'] . '</pre>';
							$q64 = $row['quantr64'];
						} else {
							$q64 = compare($row['quantr64'], $n64);
							$n64 = compare($n64, $row['quantr64']);
						}

						$classe16 = null;
						$classe32 = null;
						$classe64 = null;
						if (($row['nasm16Error'] == null || $row['nasm16Error'] == '' || strpos($row['nasm16Error'], 'warning:') !== false) && $row['nasm16'] != $row['quantr16']) {
							$classe16 = 'wrongRow';
						} else if (($row['nasm16'] == null || $row['nasm16'] == '') && $row['quantr16'] != null) {
							$classe16 = 'mis-encode';
						} else {
							$classe16 = 'correctRow';
						}
						if (($row['nasm32Error'] == null || $row['nasm32Error'] == '' || strpos($row['nasm32Error'], 'warning:') !== false) && $row['nasm32'] != $row['quantr32']) {
							$classe32 = 'wrongRow';
						} else if (($row['nasm32'] == null || $row['nasm32'] == '') && $row['quantr32'] != null) {
							$classe32 = 'mis-encode';
						} else {
							$classe32 = 'correctRow';
						}
						if (($row['nasm64Error'] == null || $row['nasm64Error'] == '' || strpos($row['nasm64Error'], 'warning:') !== false) && $row['nasm64'] != $row['quantr64']) {
							$classe64 = 'wrongRow';
						} else if (($row['nasm64'] == null || $row['nasm64'] == '') && $row['quantr64'] != null) {
							$classe64 = 'mis-encode';
						} else {
							$classe64 = 'correctRow';
						}
						?>
				<td><?= $row['id'] ?></td>
				<!-- <td><?= $row['id'] ?>, <?= $row['misEncode'] ?>, <?= $row['error'] ?></td> -->
				<td><?= $x + (($_GET['pageNo'] - 1) * $pageSize) ?></td>
				<td><?= strToLower($row['code']) ?></td>
				<td class="<?= $classe16 ?>" style="border-left: 1px solid #bfbfbf;"><?= $q16 ?></td>
				<td class="<?= $classe16 ?>"><?= $n16 ?></td>
				<td class="<?= $classe32 ?>" style="border-left: 1px solid #bfbfbf;"><?= $q32 ?></td>
				<td class="<?= $classe32 ?>"><?= $n32 ?></td>
				<td class="<?= $classe64 ?>" style="border-left: 1px solid #bfbfbf;"><?= $q64 ?></td>
				<td class="<?= $classe64 ?>"><?= $n64 ?></td>
			</tr>
		<?
				$x++;
			}
		}else if ($_GET['arch']=='RISC-V'){
			if ($_GET['selectType'] == 'error') {
				$where = " and ((not quantr32<=>substring(gas32,1,19)) or (not quantr64<=>substring(gas64,1,19)))";
			}
			if ($_GET['instruction'] != '') {
				$where .= " and SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) = '" . $_GET['instruction'] . "'";
			}else if ($_GET['search'] != '') {
				$where .= " and SUBSTRING_INDEX(test_riscv.quantrCode, ' ', 1) = '" . $_GET['search'] . "'";
			}

			$sql = "select *,
						substring(gas32,1,19) as gas32, 
						substring(gas64,1,19) as gas64, 
						(case quantr32<=>gas32 when true then 'y' else 'n' end) as c32,
						(case quantr64<=>gas64 when true then 'y' else 'n' end) as c64,
            			(case not quantr32<=>substring(gas32,1,19) when true then 'y' else 'n' end) as error32,
            			(case not quantr64<=>substring(gas64,1,19) when true then 'y' else 'n' end) as error64,
            			test_riscv.quantrCode as oriCode
						from test_riscv
						left join riscv_gas
						on test_riscv.quantrCode=riscv_gas.quantrCode
						where CI_JOB_ID=? $where
						order by test_riscv.id limit " . ($_GET['pageNo'] - 1) * $pageSize . ",$pageSize;";
			// echo $sql;
			// die;
			$stmt = mysqli_prepare($conn, $sql);

			mysqli_stmt_bind_param($stmt, "s", $_GET['CI_JOB_ID']);
			mysqli_stmt_execute($stmt);
			$result = mysqli_stmt_get_result($stmt);
			$x = 1;
			while ($row = mysqli_fetch_assoc($result)) {
				?>
				<tr>
					<?
							$row['quantr32'] = str_replace('0x', '', $row['quantr32']);
							$row['quantr64'] = str_replace('0x', '', $row['quantr64']);
							$row['gas32'] = str_replace('0x', '', $row['gas32']);
							$row['gas64'] = str_replace('0x', '', $row['gas64']);
							$quantr32 = str_replace('0x', '', $row['quantr32']);
							$quantr64 = str_replace('0x', '', $row['quantr64']);
							$gas32 = str_replace('0x', '', $row['gas32']);
							$gas64 = str_replace('0x', '', $row['gas64']);
							$myClass = null;
							if ($quantr32!=$gas32 || $quantr64!=$gas64) {
								$myClass = 'wrongRow';
							} else {
								$myClass = 'correctRow';
							}
							?>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px;" class="<?= $myClass ?>"><?= $row['id'] ?></td>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px;" class="<?= $myClass ?>"><?= $x + (($_GET['pageNo'] - 1) * $pageSize) ?></td>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px; " class="<?= $myClass ?>"><?= strToLower($row['oriCode']) ?></td>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px; white-space: nowrap; cursor:pointer;" class="<?= $myClass ?>" onclick="$('#decodeRiscVField').val('<?= $quantr32 ?>'); decodeRiscV($('#decodeRiscVField').val()); $('#riscvDecodeTab').click();" style="border-left: 1px solid #bfbfbf;"><?= $quantr32 ?></td>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px; text-align: left;" class="<?= $myClass ?>"><?= $row['quantr32Error'] ?></td>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px; text-align: left;" class="<?= $myClass ?>"><pre><?= $row['code'] ?></pre></td>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px; white-space: nowrap; cursor:pointer;" class="<?= $myClass ?>" onclick="$('#decodeRiscVField').val('<?= $gas32 ?>'); decodeRiscV($('#decodeRiscVField').val()); $('#riscvDecodeTab').click();"><?= $gas32 ?></td>
					<td style="border-left: 1px solid #bfbfbf; font-size: 16px; text-align: left; white-space: break-spaces;" nowrap class="<?= $myClass ?>"><?= $row['gas32Error'] ?></td>
                    <td style="border-left: 1px solid #bfbfbf; font-size: 16px; white-space: nowrap; cursor:pointer;" class="<?= $myClass ?>" onclick="$('#decodeRiscVField').val('<?= $quantr64 ?>'); decodeRiscV($('#decodeRiscVField').val()); $('#riscvDecodeTab').click();" style="border-left: 1px solid #bfbfbf;"><?= $quantr64 ?></td>
                    <td style="border-left: 1px solid #bfbfbf; font-size: 16px; text-align: left;" class="<?= $myClass ?>"><?= $row['quantr64Error'] ?></td>
                    <td style="border-left: 1px solid #bfbfbf; font-size: 16px; text-align: left;" class="<?= $myClass ?>"><pre><?= $row['code'] ?></pre></td>
                    <td style="border-left: 1px solid #bfbfbf; font-size: 16px; white-space: nowrap; cursor:pointer;" class="<?= $myClass ?>" onclick="$('#decodeRiscVField').val('<?= $gas64 ?>'); decodeRiscV($('#decodeRiscVField').val()); $('#riscvDecodeTab').click();"><?= $gas64 ?></td>
                    <td style="border-left: 1px solid #bfbfbf; font-size: 16px; text-align: left; white-space: break-spaces;" nowrap class="<?= $myClass ?>"><?= $row['gas64Error'] ?></td>
				</tr>
			<?
					$x++;
				}
		}
	} else if ($_GET['type'] == 'paging') {
		if ($_GET['selectType'] == 'error') {
			$where = ' and ((not quantr16<=>nasm16 and nasm16Error is not null) and (not quantr32<=>nasm32 and nasm32Error is not null) and (not quantr64<=>nasm64 and nasm64Error is not null))';
		} else if ($_GET['selectType'] == 'mis-encoded') {
			$where = ' and ((nasm16 is null and not quantr16 is null) or (nasm32 is null and not quantr32 is null) or (nasm64 is null and not quantr64 is null))';
		}
		if ($_GET['instruction'] != '') {
			$where .= " and code like '" . $_GET['instruction'] . "%'";
		}else if ($_GET['search'] != '') {
			$where .= " and code like '" . $_GET['search'] . "%'";
		}

		$sql = "select count(*) as count from test left join nasm on test.code=nasm.code where CI_JOB_ID=? $where order by test.datetime limit 0,1000;";
		$stmt = mysqli_prepare($conn, $sql);
		mysqli_stmt_bind_param($stmt, "s", $_GET['CI_JOB_ID']);
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		$row = mysqli_fetch_assoc($result);
		$count = $row['count'];
		if ($count > $pageSize) {
			?>
		<ul class="pagination">
			<li class="page-item"><a class="page-link" aria-label="Previous" onclick="reloadPipelineResultTable(1)"><span aria-hidden="true">&laquo;</span></a></li>
			<? $pageNo = 1; ?>
			<? for ($x = 0; $x < $count; $x += $pageSize) { ?>
				<li class="page-item"><a class="page-link <? if ($pageNo == $_GET['pageNo']) {
				echo "currentPageButton";
			} ?>" onclick="reloadPipelineResultTable(<?= $pageNo ?>)"><?= $pageNo ?></a></li>
				<? $pageNo++; ?>
			<? } ?>
			<li class="page-item"><a class="page-link" aria-label="Next" onclick="reloadPipelineResultTable(<?= $pageNo - 1 ?>)"><span aria-hidden="true">&raquo;</span></a></li>
		</ul>
<?
	}
}
mysqli_stmt_close($stmt);
mysqli_close($conn);

function compare($a, $b)
{
	if ($a == null) {
		return $a;
	}
	$a2 = explode(" ", $a);
	$b2 = explode(" ", $b);
	$r = "";
	for ($x = 0; $x < count($a2); $x++) {
		if ($a2[$x] == $b2[$x]) {
			$r .= $a2[$x] . ' ';
		} else {
			$r .= '<span style="background-color: #ff6b65; padding: 1px; border-radius: 3px;">' . $a2[$x] . '</span> ';
		}
	}
	return $r;
}
?>
