<?
function asmweb_init()
{
    function asmweb_shortcode($atts = [], $content = null)
    {
        ob_start();
		include('html.php');
		$content = ob_get_clean();
		return $content;
    }
    add_shortcode('asmweb', 'asmweb_shortcode');
}
add_action('init', 'asmweb_init');

function simulator_init()
{
    function simulator_shortcode($atts = [], $content = null)
    {
        ob_start();
        include('simulator.php');
		$content = ob_get_clean();
		return $content;
    }
    add_shortcode('simulator', 'simulator_shortcode');
}
add_action('init', 'simulator_init');

function rv64_init()
{
    function rv64_shortcode($atts = [], $content = null)
    {
        ob_start();
        include('rv64.php');
		$content = ob_get_clean();
		return $content;
    }
    add_shortcode('rv64', 'rv64_shortcode');
}
add_action('init', 'rv64_init');
?>
