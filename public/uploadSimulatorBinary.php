<?php
require '../../../../wp-config.php';
require('vendor/autoload.php');

use \JsonMachine\JsonMachine;

$currentUser = wp_get_current_user();
$username = $currentUser->user_login;
$cacheFileKey = "/tmp/cache_" . $username . "_dump.json";
if ($_FILES["file"] == null && !file_exists($cacheFileKey)) {
	echo "Please upload dump.json";
	die;
}

function endsWith($haystack, $needle)
{
	$length = strlen($needle);
	if (!$length) {
		return true;
	}
	return substr($haystack, -$length) === $needle;
}

$mapNames['pc'] = 'PC';
$mapNames['mhartid'] = 'MHARTID';
$mapNames['mstatus'] = 'MSTATUS';
$mapNames['mip'] = 'MIP';
$mapNames['mie'] = 'MIE';
$mapNames['mideleg'] = 'MIDELEG';
$mapNames['medeleg'] = 'MEDELEG';
$mapNames['mtvec'] = 'MTVEC';
$mapNames['mepc'] = 'MEPC';
$mapNames['mcause'] = 'MCAUSE';
$mapNames['zero'] = 'X0_ZERO';
$mapNames['ra'] = 'X1_RA';
$mapNames['sp'] = 'X2_SP';
$mapNames['gp'] = 'X3_GP';
$mapNames['tp'] = 'X4_TP';
$mapNames['t0'] = 'X5_T0';
$mapNames['t1'] = 'X6_T1';
$mapNames['t2'] = 'X7_T2';
$mapNames['s0'] = 'X8_S0';
$mapNames['fp'] = 'X8_S0';
$mapNames['s1'] = 'X9_S1';
$mapNames['a0'] = 'X10_A0';
$mapNames['a1'] = 'X11_A1';
$mapNames['a2'] = 'X12_A2';
$mapNames['a3'] = 'X13_A3';
$mapNames['a4'] = 'X14_A4';
$mapNames['a5'] = 'X15_A5';
$mapNames['a6'] = 'X16_A6';
$mapNames['a7'] = 'X17_A7';
$mapNames['s2'] = 'X18_S2';
$mapNames['s3'] = 'X19_S3';
$mapNames['s4'] = 'X20_S4';
$mapNames['s5'] = 'X21_S5';
$mapNames['s6'] = 'X22_S6';
$mapNames['s7'] = 'X23_S7';
$mapNames['s8'] = 'X24_S8';
$mapNames['s9'] = 'X25_S9';
$mapNames['s10'] = 'X26_S10';
$mapNames['s11'] = 'X27_S11';
$mapNames['t3'] = 'X28_T3';
$mapNames['t4'] = 'X29_T4';
$mapNames['t5'] = 'X30_T5';
$mapNames['t6'] = 'X31_T6';

$jdk_home = '/home/gitlab-runner/Downloads/jdk-15';
if ($_FILES["file"] == null && $_SESSION['java_h2_columns'] != null) {
	$json = JsonMachine::fromFile($cacheFileKey);
	$str = json_decode($_SESSION['java_h2_columns']);
} else {
	if (endsWith($_FILES["file"]["name"], ".zip")) {
		$zip = new ZipArchive;
		if ($zip->open($_FILES["file"]["tmp_name"]) === TRUE) {
			$filename = $zip->getNameIndex($i);
			$zip->extractTo(".");
			$zip->close();
		} else {
			echo "unzip to open";
		}
		copy($filename, $cacheFileKey);
		unlink($filename);
	} else {
		copy($_FILES["file"]["tmp_name"], $cacheFileKey);
	}
	$json = JsonMachine::fromFile($cacheFileKey);

	$temp = exec("$jdk_home/bin/java -jar H2ToJson*.jar -u sa -p fuck1234shit -j jdbc:h2:tcp://quantr.hk//root/h2_qemu_recorder/qemuRecord -s 'show columns from qemurecorder_" . $username . "'");
	echo $temp;
	$str = json_decode($temp);
	$_SESSION['java_h2_columns'] = $temp;
}

$fields = array();
for ($x = 0; $x < count($str); $x++) {
	array_push($fields, $str[$x]->COLUMN_NAME);
}

if ($_SESSION['java_h2_data'] == null || $_FILES["file"] != null) {
	$sql = "select * from qemurecorder_" . $username . " where guid='" . $_GET['guid'] . "' order by sequence limit " . $_GET['pageSize'] . " offset " . $_GET['page'] * $_GET['pageSize'] . ";";
	$temp = exec("$jdk_home/bin/java -jar H2ToJson*.jar -u sa -p fuck1234shit -j jdbc:h2:tcp://quantr.hk//root/h2_qemu_recorder/qemuRecord -s \"$sql\"");
	$str = json_decode($temp);
	$_SESSION['java_h2_data'] = $temp;
} else {
	$str = json_decode($_SESSION['java_h2_data']);
}

$simulatorRecordByPc = array();
$simulatorRecordByRow = array();
$lastRow = array();
$rowNo = 0;
for ($x =  $_GET['page'] * $_GET['pageSize']; $x < count($str); $x++) {
	$row = (array) $str[$x];
	// echo "<pre>";
	// var_dump($row);
	// echo "</pre>";

	$registers = array();
	foreach ($fields as $field) {
		if ($lastRow[$field] != $row[$field]) {
			$registers[$field] = $row[$field];
		}
		$lastRow[$field] = $row[$field];
	}

	$simulatorRecordByPc[$row['pc']] = $registers;
	$simulatorRecordByRow[$rowNo] = $registers;
	$rowNo++;
}

// echo "<pre>";
// var_dump($simulatorRecordByRow);
// echo "</pre>";
// die;

$lastRow = array();
$correct = 0;
$wrong = 0;
$x = 0;
$output = '';

echo $_GET['mode'];
try {
	foreach ($json as $e) {
		if ($x == ($_GET['page'] + 1) * $_GET['pageSize']) {
			break;
		}
		if ($x >= $_GET['page'] * $_GET['pageSize']) {
			// echo "\nx=".$x."\n";
			$element = (object)$e;
			// echo "\n";

			if ($x == $_GET['page'] * $_GET['pageSize']) {
				if ($_GET['mode'] == 'true') {
					$output .= "<thead><tr>";
					$output .= "<th></th>";
					$output .= "<th>bytes</th>";
					$output .= "<th>line</th>";
					$output .= "<th>pc</th>";
					$output .= "</thead></tr>\n";
				} else {
					$output .= "<thead><tr>";
					$output .= "<th>bytes</th>";
					foreach ($element->registers as $field => $value) {
						if ($field[0] == "x") {
							continue;
						}
						$output .= "<th>$field</th>";
					}
					$output .= "</thead></tr>\n";
				}
				$output .= "<tbody>";
			}

			$chars = array_map("chr", $element->bytes);
			$bin = join($chars);
			$hex = bin2hex($bin);

			$str = "<tr style='background-color: --backgroundColor--;'>\n";
			$str .= "<td class='px-1'>--icon--</td>\n";
			$str .= "<td class='px-1' nowrap>" . chunk_split($hex, 2, ' ') . "</td>\n";
			$str .= "<td class='px-1'>" . $element->line . "</td>\n";
			$shit = false;
			$noOfField = 0;
			foreach ($element->registers as $field => $value) {
				$register = (object)$value;
				if ($field[0] == "x") {
					// remove all x0,x1,etc ...
					continue;
				}
				// if ($field == 'id' || $field == 'guid' || $field == 'computer' || $field == 'date' || $field == 'code' || $field == 'mcycle' || $field == 'minstret' || $field == 'cycle' || $field == 'instret' || $field == 'dpc') {
				// 	continue;
				// }

				if ($_GET['mode'] == 'true') {
					if ($lastRow[$field] != $register->value) {
						$str .=  '<td class="px-1 text-center" style="background-color: #e0f0f3;">' . $field . '</td>\n';
						// $str .=  '<td>' . $simulatorRecordByRow[$x][$field]  . '</td>';
						$noOfField++;
						// if ($simulatorRecordByRow[$x][$field] != $element->$field->value) {

						// !!! don't compare value to value, because 4294965746 and -1550 also equal to hex FFFFF9F2
						$temp == false;
						$style = "";
						//if (dechex(hexdec($simulatorRecordByRow[$x][$mapNames[$field]])) != substr(dechex($register->value), -8, 8)) {
						//if (substr($simulatorRecordByRow[$x][$mapNames[$field]], -8, 8) != substr(str_pad(dechex($register->value), 8, 0, STR_PAD_LEFT), -8, 8)) {
						if (hexdec($simulatorRecordByRow[$x][$mapNames[$field]]) != $register->value) {
							// echo $simulatorRecordByRow[$x][$mapNames[$field]] . "--\n";
							// echo hexdec($simulatorRecordByRow[$x][$mapNames[$field]]) . "--\n";
							// echo $register->value . "++\n";
							// echo hexdec("0x" . $register->value) . "++\n";
							$shit = true;
							$style = "style='background-color: #ffe1e1;'";
						}
						// if ($mapNames[$field] == 'X6_T1') {
						// 	var_dump($simulatorRecordByRow[$x]);
						// 	die;
						// }
						$str .=  "<td class='px-1' " . $style . ">" . substr(str_pad(dechex($register->value), 8, 0, STR_PAD_LEFT), -8, 8) . '</td>\n';
					}
					$lastRow[$field] = $register->value;
				} else {
					if ($register->value == 0) {
						$str .=  '<td class="px-1">' . dechex($row[$field]) . '</td>\n';
					} else {
						$str .=  '<td class="px-1" style="background-color: #e0f0f3;">' . substr(dechex($register->value), -8, 8) . "</td>\n";
					}
				}
			}
			if ($_GET['mode'] == 'true' && $x < count($simulatorRecordByRow)) {
				if ($shit) {
					$str = str_replace('--icon--', "<span class='hiddenText'>shit</span><i class='fas fa-times-circle' style='color: #cc0000;'></i>\n", $str);
					$str = str_replace('--backgroundColor--', 'inherit', $str);
					$wrong++;
				} else {
					$str = str_replace('--icon--', "<i class='fas fa-check-circle' style='color: #00cc00;'></i>\n", $str);
					$str = str_replace('--backgroundColor--', 'inherit', $str);
					$correct++;
				}
			} else {
				$str = str_replace('--icon--', '', $str);
				$str = str_replace('--backgroundColor--', '', $str);
			}

			$output .= $str;

			// memory operands
			$memoryOperands = $element->memoryOperands;
			if (count($memoryOperands) > 0 && $x > 0) {
				$output .= "<td style='background-color: #a0de6d; text-align: center;'>" . count($memoryOperands) . "</td>\n";
				for ($z = 0; $z < count($memoryOperands); $z++) {
					$output .= "<td style='background-color: #d9fabe;'>" . $memoryOperands[$z]['operand'] . '<span style="font-weight: bold; color: #007eff; margin: 0px 5px;">' . str_pad(dechex($memoryOperands[$z]['b'] & 0xff), 2, 0, STR_PAD_LEFT) . '</span>' . dechex($memoryOperands[$z]['offset']) . "</td>\n";
				}
			}
			// end ,memory operands
			$output .= "</tr>\n";
		}
		$x++;
	}
} catch (Exception $e) {
	echo 'Caught exception: ',  $e->getMessage(), "\n";
	die;
}

$output .= "</tbody>";
$output .= "<script>\n";
$output .= "$('#simulatorCorrect').html('" . $correct . "');\n";
$output .= "$('#simulatorWrong').html('" . $wrong . "');\n";
$output .= "</script>\n";
echo $output;
