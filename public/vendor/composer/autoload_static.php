<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1166ba274c000b92cb3167766965e38e
{
    public static $files = array (
        'ace6d88241f812b4accb2d847454aef6' => __DIR__ . '/..' . '/halaxa/json-machine/src/functions.php',
    );

    public static $prefixLengthsPsr4 = array (
        'J' => 
        array (
            'JsonMachine\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'JsonMachine\\' => 
        array (
            0 => __DIR__ . '/..' . '/halaxa/json-machine/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1166ba274c000b92cb3167766965e38e::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1166ba274c000b92cb3167766965e38e::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit1166ba274c000b92cb3167766965e38e::$classMap;

        }, null, ClassLoader::class);
    }
}
